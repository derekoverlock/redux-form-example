import React, { Component } from 'react';
import { connect } from 'react-redux';

const DisplayForm = (props) => (
    <code>
        {JSON.stringify(props.formValues)}
    </code>
);

export default connect((state) => ({
    formValues: state.form && state.form.form && state.form.form.values
}))(DisplayForm);