import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';

import MultiCheckbox from './multi-checkbox';

class Form extends Component {

    render() {
        const invoiceItems = [
            {
                id: '68ef2cd8-df6f-4b58-abdd-cc3b7eb4d153',
                item: 'Option 1'
            },
            {
                id: '0141db6f-0d0f-4a1a-b8a0-a2f47655a13d',
                item: 'Option 2'
            },
            {
                id: 'd51014dd-8217-4501-8416-a3a484df17f3',
                item: 'Option 3'
            }
        ];

        return (
            <form>
                <Field component={MultiCheckbox} name="invoiceItems" invoiceItems={invoiceItems} />
            </form>
        );
    }

}

export default reduxForm({
    form: 'form'
})(Form);
