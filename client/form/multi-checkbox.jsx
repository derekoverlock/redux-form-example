import React, { Component } from 'react';

export default class MultiCheckbox extends Component {
    constructor(props) {
        super(props);

        this.toggleValue = this.toggleValue.bind(this);
    }

    toggleValue({ target: { checked, value: toggledValue }}) {
        const { input: { value, onChange} } = this.props;

        if (checked) {
            onChange([...value, toggledValue]);
        } else {
            onChange(value.filter((currentValue) => currentValue !== toggledValue));
        }
    }

    render() {
        const { invoiceItems } = this.props;
        return (
            <div>
                {invoiceItems.map(invoiceItem =>
                    <div key={invoiceItem.id}>
                        <input type="checkbox" value={invoiceItem.id} onChange={this.toggleValue} /> {invoiceItem.item}
                    </div>
                )}
            </div>
        );
    }
}