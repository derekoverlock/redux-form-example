import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store from './store';
import Form from './form';
import DisplayForm from './form/display-form';

const bootstrap = document.createElement('div');

document.body.appendChild(bootstrap);

ReactDOM.render(<Provider store={store}>
    <div>
        <Form />
        <DisplayForm/>
    </div>
</Provider>, bootstrap);
